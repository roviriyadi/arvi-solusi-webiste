import React from 'react';
import { useState, useEffect } from 'react';
import axios from 'axios';
import Link from 'next/link';
import Navbar from "../components/Navbar";
import Banner from '../components/Banner';
import PopupContact from '../components/PopupContact';
import Product from '../components/Product';
import Feature from '../components/Feature';
import Footer from '../components/Footer';
import CustomerFeedback from '../components/CustomerFeedback';

// const NewsLink = ({ slug, title }) => (
//     <Link href="/news/[slug]" as={`/news/${slug}`}>
//         <a>{title}</a>
//     </Link>
// )

// const getNews = () => {
//     return [
//         { title: "Berita Satu", slug: "news1" },
//         { title: "Berita Dua", slug: "news2" }
//     ];
// }

const Index = (props) => {
    // const [joke, setJoke] = useState('');
    // useEffect(() => {
    //     getJoke();
    // }, []);

    // const getJoke = async () => {
    //     const res = await axios.get('https://api.chucknorris.io/jokes/random');
    //     setJoke(res.data.value);
    // };

    return (
        <>
            <Navbar/>
            <Banner/>
            <div className="content-wrapper">
                <div className="container">
                    <Feature/>
                    <Product/>
                    <CustomerFeedback/>
                </div>
            </div>
            <PopupContact/>
            <Footer/>
        </>
    );
};

// export const getStaticProps = async () => {
//     const res = await axios.get('https://api.chucknorris.io/jokes/random');
//     return {
//         props: {
//             value: res.data.value
//         }
//     };
// };

export default Index;