import { Component } from 'react';
// import Document, { Main, NextScript, DocumentContext } from 'next/document'
import MyHead from '../components/Head';
import Script from '../components/Script';

const MyApp = ({ Component, pageProps }) => {

    // static async getInitialProps(ctx) {
    //     const initialProps = await Document.getInitialProps(ctx);
    //     let props = { ...initialProps };
    //     return props;
    // }

    return (
        <div>
            <MyHead />
            <section id="body" data-spy="scroll" data-target=".navbar" data-offset="100">
                <Component {...pageProps} />
                {/* <script src="vendors/js/jquery.min.js"></script>
                <script src="vendors/js/bootstrap-5.min.js" async defer></script>
                <script src="vendors/js/owl.carousel.min.js" async defer></script> */}
                <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossOrigin="anonymous"></script>
                <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossOrigin="anonymous" async defer></script>
                <script src="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/js/bootstrap.min.js" integrity="sha384-oesi62hOLfzrys4LxRF63OJCXdXDipiYWBnvTl9Y9/TRlw5xlKIEHpNyvvDShgf/" crossOrigin="anonymous" async defer></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js" integrity="sha512-bPs7Ae6pVvhOSiIcyUClR7/q2OAsRiovw4vAkX+zJbw3ShAeeqezq50RIIcIURq7Oa20rW2n2q+fyXBNcU9lrw==" crossOrigin="anonymous" async defer></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.js" integrity="sha512-A7AYk1fGKX6S2SsHywmPkrnzTZHrgiVT7GcQkLGDe2ev0aWb8zejytzS8wjo7PGEXKqJOrjQ4oORtnimIRZBtw==" crossOrigin="anonymous" async defer></script>
                <script src="vendors/js/landingpage.js" async defer></script>
                {/* <Script/> */}
            </section>
        </div>
    );
}

export default MyApp;