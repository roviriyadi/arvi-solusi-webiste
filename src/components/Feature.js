import React from 'react';

export default () => {
    return (
        <section className="features-overview" id="features-section">
            <div className="content-header">
                <h2>Kami Menjamin</h2>
                <h6 className="section-subtitle text-muted">Tidak perlu khawatir dengan kondisi mobil di showroom kami<br />Mobil
              yang kami jual sangat berkualitas dan dalam kondisi prima.</h6>
            </div>
            <div className="d-md-flex justify-content-between">
                <div className="grid-margin d-flex justify-content-start">
                    <div className="features-width">
                        <img src="images/Group12.svg" alt="" className="img-icons" />
                        <h5 className="py-3">Speed<br />Optimisation</h5>
                        <p className="text-muted">Lorem ipsum dolor sit amet, tincidunt vestibulum. Fusce egeabus consectetuer
                  turpis, suspendisse.</p>
                        <a href="#">
                            <p className="readmore-link">Readmore</p>
                        </a>
                    </div>
                </div>
                <div className="grid-margin d-flex justify-content-center">
                    <div className="features-width">
                        <img src="images/Group7.svg" alt="" className="img-icons" />
                        <h5 className="py-3">SEO and<br />Backlinks</h5>
                        <p className="text-muted">Lorem ipsum dolor sit amet, tincidunt vestibulum. Fusce egeabus consectetuer
                  turpis, suspendisse.</p>
                        <a href="#">
                            <p className="readmore-link">Readmore</p>
                        </a>
                    </div>
                </div>
                <div className="grid-margin d-flex justify-content-end">
                    <div className="features-width">
                        <img src="images/Group5.svg" alt="" className="img-icons" />
                        <h5 className="py-3">Content<br />Marketing</h5>
                        <p className="text-muted">Lorem ipsum dolor sit amet, tincidunt vestibulum. Fusce egeabus consectetuer
                  turpis, suspendisse.</p>
                        <a href="#">
                            <p className="readmore-link">Readmore</p>
                        </a>
                    </div>
                </div>
            </div>
        </section>
    );
}