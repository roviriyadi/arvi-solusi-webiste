import React from 'react';
import Head from 'next/head';

const MyHead = () => {
    return (
        <Head>
            {/* <title>Arvi Solusi</title>
            <meta name="description" content="Solusi Alternatif Virtualisasi" /> */}
            <title>Intan Garasi</title>
            <meta name="description" content="Intan Garasi Spesialis Mobil Banjir" />
            <meta name="Intan Garasi Spesialis Mobil Banjir" content="Mobil terendam banjir? Jangan khawatir ada Intan Garasi Spesialis Mobil Banjir." />
            <meta name="viewport" content="width=device-width, initial-scale=1.0" />
            <meta charSet="UTF-8" />
            <meta httpEquiv="X-UA-Compatible" content="ie=edge" />
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" integrity="sha512-tS3S5qG0BlhnQROyJXvNjeEM4UpMXHrQfTGmbQ1gKmelCxlSEBUaxhRBj/EFTzpbP4RVSrpEikbmdJobCvhE3g==" crossOrigin="anonymous" />
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.css" integrity="sha512-OTcub78R3msOCtY3Tc6FzeDJ8N9qvQn1Ph49ou13xgA9VsH9+LRxoFU6EqLhW4+PKRfU+/HReXmSZXHEkpYoOA==" crossOrigin="anonymous" />
            {/* <link rel="stylesheet" href="vendors/css/owl.carousel.min.css" /> */}
            {/* <link rel="stylesheet" href="vendors/css/owl.theme.default.css" /> */}
            {/* <link rel="stylesheet" href="vendors/css/materialdesignicons.min.css" /> */}
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/5.6.55/css/materialdesignicons.min.css" integrity="sha512-75gimAGx0NOGihrwAtl3xq9SUgq1a3NtIe9fTBtrHOajqMEHCNLZI/BMVE9oMSG3ms5sVira5A2coilfdmxfjA==" crossOrigin="anonymous" />
            {/* <link rel="stylesheet" href="vendors/css/aos.css" /> */}
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.css" integrity="sha512-1cK78a1o+ht2JcaW6g8OXYwqpev9+6GqOkz9xmBN9iUUhIndKtxwILGWYOSibOKjLsEdjyjZvYDq/cZwNeak0w==" crossOrigin="anonymous" />
            <link rel="stylesheet" href="vendors/css/style.min.css" />
            {/* <link rel="stylesheet" href="vendors/css/bootstrap.min.css" /> */}
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/css/bootstrap.min.css" integrity="sha384-r4NyP46KrjDleawBgD5tp8Y7UzmLA05oM1iAEQ17CSuDqnUK2+k9luXQOfXJCJ4I" crossOrigin="anonymous"></link>
        </Head>
    )
}

export default MyHead;