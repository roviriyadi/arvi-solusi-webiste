import React from 'react';

const PopupContact = () => {

    const [customer, setCustomer] = React.useState({
        name: "",
        phone: "",
        email: "",
        message: ""
    });

    const onChange = React.useCallback((el) => {
        const val = el.target.value;
        setCustomer({ ...customer, [el.target.id]: val });
    });

    const onSubmit = React.useCallback(() => {
        console.log("XXX", customer);
    }, [customer]);

    return (
        <div className="modal fade" id="exampleModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div className="modal-dialog" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <h4 className="modal-title" id="exampleModalLabel">Hubungi Kami</h4>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        <form>
                            <input type="text" className="form-control mb-2" id="name" placeholder="Nama Lengkap" value={customer.name} onChange={onChange}/>
                            <input type="text" className="form-control mb-2" id="phone" placeholder="No. Telpon/HP" value={customer.phone} onChange={onChange}/>
                            <input type="email" className="form-control mb-2" id="email" placeholder="Email" value={customer.email} onChange={onChange}/>
                            <textarea className="form-control" id="message" placeholder="Isi pesan" onChange={onChange} value={customer.message}/>
                        </form>
                    </div>
                    <div className="modal-footer" style={{ paddingRight: 39, justifyContent: "flex-end" }}>
                        {/* <button type="button" className="btn btn-light" data-dismiss="modal">Tutup</button> */}
                        <button type="button" className="btn btn-success" onClick={onSubmit}>Kirim</button>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default PopupContact;