import React from 'react';

const Footer = () => {
  return (
    <footer className="border-top bg-dark fixed-bottom mt-auto">
      <p className="text-center text-warning pt-2">Copyright © 2020<a href="https://intangarasi.com"
        className="px-1 text-light">Intan Garasi.</a>All rights reserved.</p>
    </footer>
  );
};

export default Footer;