const CustomerFeedback = () => {
    return (
        <section className="customer-feedback" id="feedback-section" >
            <div className="row">
                <div className="col-12 text-center pb-5">
                    <h2>Testimoni</h2>
                </div>
                <div className="owl-carousel owl-theme grid-margin">
                    <div className="card customer-cards">
                        <div className="card-body">
                            <div className="text-center">
                                <img src="images/face15.jpg" width="89" height="89" alt="" className="img-customer" />
                                <p className="m-0 py-3 text-muted">Lorem ipsum dolor sit amet, tincidunt vestibulum. Fusce egeabus
                     consectetuer turpis, suspendisse.</p>
                                <div className="content-divider m-auto"></div>
                                <h6 className="card-title pt-3">Cody Lambert</h6>
                                <h6 className="customer-designation text-muted m-0">Marketing Manager</h6>
                            </div>
                        </div>
                    </div>
                    <div className="card customer-cards">
                        <div className="card-body">
                            <div className="text-center">
                                <img src="images/face16.jpg" width="89" height="89" alt="" className="img-customer" />
                                <p className="m-0 py-3 text-muted">Lorem ipsum dolor sit amet, tincidunt vestibulum. Fusce egeabus
                     consectetuer turpis, suspendisse.</p>
                                <div className="content-divider m-auto"></div>
                                <h6 className="card-title pt-3">Cody Lambert</h6>
                                <h6 className="customer-designation text-muted m-0">Marketing Manager</h6>
                            </div>
                        </div>
                    </div>
                    <div className="card customer-cards">
                        <div className="card-body">
                            <div className="text-center">
                                <img src="images/face1.jpg" width="89" height="89" alt="" className="img-customer" />
                                <p className="m-0 py-3 text-muted">Lorem ipsum dolor sit amet, tincidunt vestibulum. Fusce egeabus
                     consectetuer turpis, suspendisse.</p>
                                <div className="content-divider m-auto"></div>
                                <h6 className="card-title pt-3">Tony Martinez</h6>
                                <h6 className="customer-designation text-muted m-0">Marketing Manager</h6>
                            </div>
                        </div>
                    </div>
                    <div className="card customer-cards">
                        <div className="card-body">
                            <div className="text-center">
                                <img src="images/face2.jpg" width="89" height="89" alt="" className="img-customer" />
                                <p className="m-0 py-3 text-muted">Lorem ipsum dolor sit amet, tincidunt vestibulum. Fusce egeabus
                     consectetuer turpis, suspendisse.</p>
                                <div className="content-divider m-auto"></div>
                                <h6 className="card-title pt-3">Tony Martinez</h6>
                                <h6 className="customer-designation text-muted m-0">Marketing Manager</h6>
                            </div>
                        </div>
                    </div>
                    <div className="card customer-cards">
                        <div className="card-body">
                            <div className="text-center">
                                <img src="images/face3.jpg" width="89" height="89" alt="" className="img-customer" />
                                <p className="m-0 py-3 text-muted">Lorem ipsum dolor sit amet, tincidunt vestibulum. Fusce egeabus
                     consectetuer turpis, suspendisse.</p>
                                <div className="content-divider m-auto"></div>
                                <h6 className="card-title pt-3">Sophia Armstrong</h6>
                                <h6 className="customer-designation text-muted m-0">Marketing Manager</h6>
                            </div>
                        </div>
                    </div>
                    <div className="card customer-cards">
                        <div className="card-body">
                            <div className="text-center">
                                <img src="images/face20.jpg" width="89" height="89" alt="" className="img-customer" />
                                <p className="m-0 py-3 text-muted">Lorem ipsum dolor sit amet, tincidunt vestibulum. Fusce egeabus
                     consectetuer turpis, suspendisse.</p>
                                <div className="content-divider m-auto"></div>
                                <h6 className="card-title pt-3">Cody Lambert</h6>
                                <h6 className="customer-designation text-muted m-0">Marketing Manager</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
}

export default CustomerFeedback;