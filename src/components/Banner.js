import React from 'react';

const Banner = () => {
    return (
        <div className="container">
            <div id="carouselExampleIndicators" className="carousel slide" data-ride="carousel" data-interval="3000">
                <ol className="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" className="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                </ol>
                <div className="carousel-inner" style={{ height: "auto" }}>
                    <div className="carousel-item active">
                        <img className="d-block w-100" alt="First slide" src="images/welcome-image.jpg" />
                        <div className="carousel-caption d-none d-md-block">
                            <h5>Promo Bulan ini</h5>
                            <p>DP hanya 20%</p>
                        </div>
                    </div>
                    <div className="carousel-item">
                        <img className="d-block w-100" alt="Second slide" src="images/foto-showroom-1.jpg" />
                        <div className="carousel-caption d-none d-md-block">
                            <h5>Promo Bulan ini</h5>
                            <p>Gratis perawatan 1 Tahun</p>
                        </div>
                    </div>
                </div>
                <a className="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span className="sr-only">Previous</span>
                </a>
                <a className="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span className="carousel-control-next-icon" aria-hidden="true"></span>
                    <span className="sr-only">Next</span>
                </a>
            </div>
        </div>
    );
}

export default Banner;