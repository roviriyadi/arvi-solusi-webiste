import React from 'react';
import Link from 'next/link';

const Navbar = () => {
  return (
    <header id="header-section">
      <nav className="navbar navbar-dark bg-dark navbar-expand-lg fixed-top pl-3 pr-3" id="navbar">
        <div className="container">
          <div className="navbar-brand-wrapper d-flex w-100">
            <img src="images/Group2.svg" alt=""></img>
            <button className="navbar-toggler ml-auto" type="button" data-toggle="collapse"
              data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
              aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>
          </div>
          <div className="collapse navbar-collapse navbar-menu-wrapper" id="navbarSupportedContent">
            <ul className="navbar-nav align-items-lg-center align-items-start ml-auto">
              <li className="d-flex align-items-center justify-content-between pl-4 pl-lg-0">
                <div className="navbar-collapse-logo">
                  <img src="images/Group2.svg" alt=""></img>
                </div>
                <button className="navbar-toggler close-button" type="button" data-toggle="collapse"
                  data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                  aria-label="Toggle navigation">
                  <span className="mdi mdi-close navbar-toggler-icon pl-5 text-dark"></span>
                </button>
              </li>
              <li className="nav-item">
                <a className="nav-link text-light" href="#header-section">Beranda <span className="sr-only">(current)</span></a>
              </li>
              <li className="nav-item">
                <a className="nav-link text-light" href="#features-section">Layanan</a>
              </li>
              <li className="nav-item">
                <a className="nav-link text-light" href="#digital-marketing-section">Tentang</a>
              </li>
              <li className="nav-item">
                <a className="nav-link text-light" href="#feedback-section">Testimoni</a>
              </li>
              <li className="nav-item btn-contact-us pl-4 pl-lg-0">
                <button className="btn btn-info" data-toggle="modal" data-target="#exampleModal">Hubungi Kami</button>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </header>
  );
};

export default Navbar;